<h1>Hello World from Page Controller</h1>
<p>{{ $front_end }}</p>


@if(count($topics) > 0)
	@foreach($topics as $topic)
		<li>{{ $topic }}</li>
	@endforeach
@else
	<p>Nothing to display</p>
@endif