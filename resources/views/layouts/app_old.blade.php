<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="{{asset('/css/app.css')}}">

	<title>Laravel</title>
</head>
<body>
	<!-- navbar -->
	@include('inc.navbar')

	<div class="container">
		@include('inc/messages')
		@yield('content')
	</div>
</body>
</html>
