@extends('layouts/app_old')

@section('content')
    <h1>Posts</h1>

    <!-- <li>{{$posts}}</li> -->

    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div>
                <h3><a href='/posts/{{$post->id}}'>{{$post->title}}</a></h3>
                <small>Written on {{$post->created_at}}</small>
                <hr>
            </div>
        @endforeach
        {{ $posts->links() }}
    @else
        <p>No posts</p>
    @endif
@endsection

